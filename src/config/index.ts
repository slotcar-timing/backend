import * as dotenv from 'dotenv'

process.env.NODE_ENV = process.env.NODE_ENV ?? 'development'

const envFound = dotenv.config()
if (envFound.error != null) {
  throw new Error('.env file not found')
}

export default {
  /**
     * The host of the MQTT broker
     */
  mqttHost: process.env.MQTT_HOST || 'localhost',

  /**
     * The port to serve the Express api on
     */
  httpPort: parseInt(process.env.HTTP_PORT || '1337'),

  /**
   * Winston's log level
   */
  logs: {
    level: process.env.LOG_LEVEL || 'info'
  },

  /**
   * Redis Host
   */
  redisHost: process.env.REDIS_HOST || 'localhost',

  /**
   * Redis port
   */
  redisPort: parseInt(process.env.REDIS_PORT) || 6379

}

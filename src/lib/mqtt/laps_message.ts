export class LapsMessage {
  lane: number
  count: bigint
  millis: bigint

  constructor(lane: number, count: bigint, millis: bigint) {
    this.lane = lane
    this.count = count
    this.millis = millis
  }
}

export class BeamMessage {
  lane: number
  sequence: bigint
  timestamp: bigint

  constructor(lane: number, sequence: bigint, timestamp: bigint) {
    this.lane = lane
    this.sequence = sequence
    this.timestamp = timestamp
  }
}

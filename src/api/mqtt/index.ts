import { Client } from 'mqtt'
import Logger from '../../loaders/logger'
import BeamCrossedService from '../../services/beam_crossed'
import { BeamMessage } from '../../lib/mqtt/beam_message'
import 'reflect-metadata'
import { container } from 'tsyringe'

const beam_broken_topic = 'slotcars/beam_broken/+'

export default function(client: Client) {
  client.subscribe(beam_broken_topic, function(err) {
    if (!err) {
      Logger.info(`MQTT: Subscribed to ${beam_broken_topic}`)
    }
  })

  client.on('message', function(topic, message) {
    // Logger.info(`MQTT: Received MSG: ${message} in topic: ${topic}`)
    const lane = parseInt(topic.split('/').pop())
    const messageObj = JSON.parse(message.toString())
    const themessage = new BeamMessage(lane, messageObj.sequence, messageObj.timestamp)
    container.resolve(BeamCrossedService).logTimestamp(themessage)
  })
  return client
}

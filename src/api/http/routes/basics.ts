import { Router, Request, Response } from 'express'

export default function(app: Router): void {
  app.get('/hello', function(req: Request, res: Response) {
    res.send('Hello World!')
  })
}

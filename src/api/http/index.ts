import { Router } from 'express'
import basics from './routes/basics'

export default function(): Router {
  const app = Router()
  basics(app)
  return app
}

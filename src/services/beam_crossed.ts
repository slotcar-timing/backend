import { Logger } from 'winston'
import { BeamMessage } from '../lib/mqtt/beam_message'
import events from '../subscribers/events'
import { LapsMessage } from '../lib/mqtt/laps_message'
import { EventEmitter } from 'events'
import { inject, injectable, singleton } from 'tsyringe'

@injectable()
@singleton()
export default class BeamCrossedService {
  lastMessages: Record<number,BeamMessage>
  emitter: EventEmitter
  logger: Logger

  constructor(
    @inject('eventEmitter') emitter: EventEmitter,
    @inject('logger') logger: Logger
  ) {
    this.lastMessages = {}
    this.logger = logger
    this.emitter = emitter
  }


  public logTimestamp(message: BeamMessage) : void {
    const lane = message.lane
    const lastMessage = this.lastMessages[lane]
    if(lastMessage == null) {
      this.lastMessages[lane] = message
      this.logger.info(`Beam crossed on Lane ${lane}. Number: ${message.sequence}, Timestamp: ${message.timestamp}`)
      return
    }

    if(message.sequence > lastMessage.sequence){
      const lapsElapsed = message.sequence - lastMessage.sequence
      const laptimeInMillis = (message.timestamp - lastMessage.timestamp) / lapsElapsed
      this.logger.log('debug', `Got valid laptime message on lane ${lane}`)
      this.emitter.emit(events.laps.completed, new LapsMessage(lane, lapsElapsed, laptimeInMillis))
      this.lastMessages[lane] = message
      return
    } else {
      this.logger.info(`Got message for Lane ${lane}, but discarded it since it's out of order`)
    }
  }
}

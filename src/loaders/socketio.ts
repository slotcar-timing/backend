import { Server, Socket } from 'socket.io'
import * as http from 'http'
import Logger from './logger'

export default function(httpServer: http.Server): Server {
  const io = new Server({
    cors: { methods: ["GET", "POST"] }
  }).listen(
    httpServer,
  )
  io.on("connection", function(socket: Socket) {
    Logger.info(`New SocketIO connection from ${socket}`)
  })
  return io
}

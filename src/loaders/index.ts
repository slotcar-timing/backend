import expressLoader from './express'
import mqttLoader from './mqtt'
// import wssLoader from './websocket'
import socketioLoader from './socketio'
import { createServer } from 'http'
import Logger from './logger'
import config from '../config'
import { AddressInfo } from 'net'
import { container } from 'tsyringe'
import { EventEmitter } from 'events'
import setupEvents from './events'

export default function(): void {
  container.register('logger', { useValue: Logger })
  container.register('eventEmitter', { useValue: new EventEmitter()})
  const app = expressLoader()
  Logger.info('Express loaded')

  mqttLoader()
  Logger.info('MQTT loaded')

  const httpServer = createServer(app)

  // const wss = wssLoader(httpServer)
  // container.register('wss', { useValue: wss })
  // Logger.info('Websockets set up')

  const sio = socketioLoader(httpServer)
  container.register('sio', { useValue: sio })
  Logger.info("Socket.IO set up")

  setupEvents()

  httpServer.listen(config.httpPort, function() {
    const addrinfo = (httpServer.address() as AddressInfo)
    Logger.info(`HTTP server listening on Port ${addrinfo.port}`)
  })
}

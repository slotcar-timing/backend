import * as mqtt from 'mqtt'
import Logger from './logger'
import config from '../config'
import triggers from '../api/mqtt'

export default function(): mqtt.Client {
  const mqttAddress = `mqtt://${config.mqttHost}`
  const client = mqtt.connect(mqttAddress)

  client.on('connect', function() {
    Logger.info(`MQTT connected to ${mqttAddress}`)
  })

  triggers(client)

  return client
}

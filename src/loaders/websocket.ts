import * as http from 'http'
import * as WebSocket from 'ws'
import Logger from './logger'

export default function(httpserver: http.Server): WebSocket.Server {
  const wss = new WebSocket.Server({ server: httpserver })

  wss.on('connection', function(ws: WebSocket) {
    Logger.info(`New WSS connection from ${ws}`)
  })

  return wss
}

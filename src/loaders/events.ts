import { EventEmitter } from "events"
import { container } from "tsyringe"
import events from "../subscribers/events"
import logLaps from "../subscribers/laps/log_completed_laps"
// import wssLaps from '../subscribers/laps/ws_broadcast_laps'
import sioBroadcastLaps from '../subscribers/laps/sio_broadcast_laps'

export default function registerListeners(): void {
  const emitter:EventEmitter = container.resolve('eventEmitter')
  emitter.on(events.laps.completed, logLaps)
  // emitter.on(events.laps.completed, wssLaps)
  emitter.on(events.laps.completed, sioBroadcastLaps)
}

import { container } from 'tsyringe'
import { Logger } from 'winston'
import { LapsMessage } from '../../lib/mqtt/laps_message'

export default function(laps: LapsMessage):void {
  const logger: Logger = container.resolve('logger')
  logger.info(`Lane ${laps.lane}: ${laps.count} Laps completed. Lap time: ${laps.millis} ms`)
}

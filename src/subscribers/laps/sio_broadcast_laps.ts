import { container } from 'tsyringe'
import { LapsMessage } from '../../lib/mqtt/laps_message'

export default function(laps: LapsMessage): void {
  const sio: SocketIO.Server = container.resolve('sio')
  sio.emit('laps_completed', JSON.stringify(laps))
}

import { container } from 'tsyringe'
import { Logger } from 'winston'
import { LapsMessage } from '../../lib/mqtt/laps_message'
import * as WebSocket from 'ws'

export default function(laps: LapsMessage): void {
  const logger: Logger = container.resolve('logger')
  const wss: WebSocket.Server = container.resolve('wss')
  wss.clients.forEach(function(client) {
    if(client.readyState === WebSocket.OPEN){
      client.send(JSON.stringify(laps))
    }
  })
}

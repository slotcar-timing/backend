FROM node:alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY build/ .

RUN touch .env

ENV MQTT_HOST=localhost
ENV HTTP_PORT=8888

CMD [ "node", "app.js"]
